def reverse_nums(arr):
    new_arr = []
    for i in range(len(arr), 0, -1):
        new_arr.append(arr[i - 1])
    return new_arr

print(reverse_nums([1, 2, 3, 4, 5]))
