def arrange_nums(side_length):
    if side_length < 32 and side_length > 0:
        result = ""
        for i in range(1, side_length+1):
            new_line = ""
            for j in range(1, side_length+1):
                if i % 2 == 0:
                    new_line += "{:>4}".format(str(i * side_length - (j - 1)))
                else:
                    new_line += "{:>4}".format(str((i - 1) * side_length + j))
            result += new_line + "\n"
        return result
    else:
        return "Too big or small number"
