def find_most_frequent(input):
    most_frequent = 1

    for i in range(len(input)):
        counter = 1
        for j in range(len(input)):
            if j != i:
                if input[i] == input[j]:
                    counter += 1
        if counter > most_frequent:
            most_frequent = counter
    return most_frequent

print(str(find_most_frequent("программа")))
