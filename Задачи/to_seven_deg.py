def to_seven_deg(user_input):
    result = ""
    table = [" _ ", "   ", " _ ", " _ ", "   ", " _ ", " _ ", " _ ", " _ ", " _ ",
             "| |", "  |", " _|", " _|", "|_|", "|_ ", "|_ ", "  |", "|_|", "|_|",
             "|_|", "  |", "|_ ", " _|", "  |", " _|", "|_|", "  |", "|_|", " _|",
    ]
    input = str(user_input)

    for i in range(0, 3):
        for j in range(0, len(input)):
            result += table[int(input[j]) + i * 10]
        result += "\n"

    return result
