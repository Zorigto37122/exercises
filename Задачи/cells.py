def create_table(cols, rows):
    result_table = ""
    #creating new row, that we will copy on num of cols
    new_row = ""
    for i in range(0, cols):
        new_row += "+--"
    #Add tail
    new_row += "+\n"
    for i in range(0, cols):
        new_row += "|  "
    #Add tail
    new_row += "|\n"
    #Copying rows
    for i in range(0, rows):
        result_table += new_row
    #Add tail
    for i in range(0, cols):
        result_table += "+--"
    #Add tail
    result_table += "+"
    return result_table

my_table = create_table(20, 10)
print(my_table)
